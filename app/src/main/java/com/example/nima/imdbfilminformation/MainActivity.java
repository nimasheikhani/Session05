package com.example.nima.imdbfilminformation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.security.PublicKey;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    TextView movieName;
    TextView director;
    TextView searchBox;
    ImageView moviePoster;
    TextView actors;
    TextView year;
    TextView genre;
    TextView plot;
    TextView runTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BindView();
    }

    private void BindView() {
        movieName = (TextView) findViewById(R.id.movieName);
        director = (TextView) findViewById(R.id.director);
        searchBox = (TextView) findViewById(R.id.searchBox);
        actors = (TextView) findViewById(R.id.actors);
        year = (TextView) findViewById(R.id.year);
        genre = (TextView) findViewById(R.id.genre);
        plot = (TextView) findViewById(R.id.plot);
        runTime = (TextView) findViewById(R.id.runTime);
        moviePoster = (ImageView) findViewById(R.id.moviePoster);
        (findViewById(R.id.search_button)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.search_button) {
            String movie = searchBox.getText().toString();
            if (movie.length() == 0)
                Toast.makeText(mContext,"Please Enter Movie Name", Toast.LENGTH_LONG);
            else {    // callThreadForWebService(city);
                getDataByAsyncHTTP(movie);
                searchBox.setText("");
            }
        }
    }

    void getDataByAsyncHTTP(String movie) {
        final String url = "http://www.omdbapi.com/?t="
                + movie + "&apikey=70ad462a";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                getValuesFromJSON(responseString);
            }
        });
    }

    private void getValuesFromJSON(String result) {
        try {
            JSONObject allObj = new JSONObject(result);

            movieName.setText(allObj.getString("Title"));
            year.setText("Year: " + allObj.getString("Year"));
            runTime.setText("RunTime: " + allObj.getString("Runtime"));
            actors.setText("Actors: " + allObj.getString("Actors"));
            genre.setText("Genre: " + allObj.getString("Genre"));
            plot.setText("Plot: " + allObj.getString("Plot"));
            director.setText("Director: " + allObj.getString("Director"));
            Picasso.with(mContext).load(allObj.getString("Poster")).into(moviePoster);
        } catch (Exception ex) {
            Toast.makeText(mContext,"Please Enter Movie Name", Toast.LENGTH_LONG);
        }
    }
}
